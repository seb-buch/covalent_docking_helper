#!/bin/env python
"""Helper script for running covalent docking with GOLD."""
####
# Description:
# Utility script to:
# 1. prepare GOLD conf files for covalent docking.
# 2. Submit the docking jobs to the queue managing system
# 3. Process the docked poses to build the covalent complexes
#
# Author:
# - Sébastien Buchoux <sebastien.buchoux@evotec.com>
####
import argparse
import glob
import logging
import math
import os
import stat
import sys
from collections import OrderedDict
from pathlib import Path
from platform import platform
from typing import Dict, List, NoReturn, Optional, Tuple, Union

print("Loading CCDC Python API...", end="", flush=True)
try:
    del os.environ["QM_IM_MODULE"]
except KeyError:
    pass

try:
    import ccdc  # type: ignore
    from ccdc.docking import Docker, Entry, Protein  # type: ignore
    from ccdc.io import EntryReader, EntryWriter, MoleculeReader  # type: ignore
    from ccdc.molecule import Atom  # type: ignore
    from ccdc.search import MoleculeSubstructure, SubstructureSearch  # type: ignore
except ImportError as exc:
    print(" FAILED")
    raise ImportError(
        "Could not find CCDC Python API. Try 'module load csds' before running this script."
    ) from exc
else:
    print(" Loaded!")

__version__ = "1.2"
__author__ = "Sébastien Buchoux <sebastien.buchoux@evotec.com>"

logger = logging.getLogger(__name__)

handler = logging.StreamHandler()
handler.setFormatter(
    logging.Formatter(
        "[%(asctime)s %(levelname)-7s] %(message)s", datefmt="%y-%m-%d %H:%M:%S"
    )
)
logger.addHandler(handler)
logger.setLevel(logging.INFO)

logger.info(
    """Platform:                     %s

Python exe:                   %s
Python version:               %s

CSD version:                  %s
CSD directory:                %s
API version:                  %s

CSDHOME:                      %s
CCDC_LICENSING_CONFIGURATION: %s
""",
    platform(),
    sys.executable,
    ".".join(str(x) for x in sys.version_info[:3]),
    ccdc.io.csd_version(),
    ccdc.io.csd_directory(),
    ccdc.__version__,
    os.environ.get("CSDHOME", "Not set"),
    os.environ.get("CCDC_LICENSING_CONFIGURATION", "Not set"),
)

logger.info("Helper script version: %s", __version__)

_GOLD_OPTIONS_PATHLIKE = [
    "cavity_file",
    "ligand_data_file",
    "directory",
    "covalent_substructure_filename",
    "protein_datafile",
]


def get_formatter_from_max_value(max_value: int) -> str:
    """Generate zero-padded string formatter from a given maximum value.

    Args:
        max_value (int): the desired max value

    Returns:
        str: The string formatter

    """
    return f"0{int(math.log10(max_value)) + 1}d"


class GoldConfiguration:
    """Class used to store information about GOLD configuration."""

    class GoldParameter:
        """Class used to store a GOLD configuration parameter."""

        def __init__(self, name: str, values: List[str], needs_equal: bool = True):
            """Create a GOLD parameter."""
            self.name = name
            self.values = list(val for val in values)
            self.needs_equal = needs_equal

        def copy(self):
            """Create a copy of self."""
            return GoldConfiguration.GoldParameter(
                self.name, self.values, self.needs_equal
            )

        def __str__(self):
            formatted_values = " ".join(self.values)

            if self.needs_equal:
                return f"{self.name} = {formatted_values}"

            return f"{self.name} {formatted_values}"

    def __init__(self, filename: Union[str, Path]):
        """Create a GOLD configuration from a GOLD configuration file."""
        self.filename = Path(filename)
        self.sections: Dict[str, List[GoldConfiguration.GoldParameter]] = OrderedDict()
        self.parameters: Dict[str, GoldConfiguration.GoldParameter] = {}

        if filename is not None:
            self.load(filename)

    def load(self, filename: Union[str, Path]):
        """Load GOLD configuration and populate self."""
        self.filename = Path(filename)

        current_section = "UNKNOWN"

        with open(filename, encoding="utf-8") as filehandler:
            for line in filehandler:
                line = line.rstrip()
                if line == "":
                    continue

                if line.startswith("  "):
                    current_section = line.strip()
                else:
                    splitted_line = line.split("=")
                    needs_equal = True

                    if len(splitted_line) == 1:
                        splitted_line = line.split(" ", 1)
                        needs_equal = False

                    name = splitted_line[0].strip()

                    values = [val.strip() for val in splitted_line[1].split()]

                    parameter = GoldConfiguration.GoldParameter(
                        name, values, needs_equal
                    )

                    self.parameters[name] = parameter
                    try:
                        self.sections[current_section].append(parameter)
                    except KeyError:
                        self.sections[current_section] = [
                            parameter,
                        ]

    def __getitem__(self, item: str):
        return self.parameters[item].values

    def __setitem__(self, key: str, value: List[str]):
        self.parameters[key].values = list(val for val in value)

    def to_string(self) -> str:
        """Convert self to an actual GOLD configuration file content."""
        value = "  GOLD CONFIGURATION FILE\n"

        for name, parameters in self.sections.items():
            value += f"\n  {name}\n"

            for parameter in parameters:
                value += f"{parameter}\n"

        return value

    def copy(self):
        """Create a copy of self."""
        other = GoldConfiguration(self.filename)

        for name, params in self.sections.items():
            other.sections[name] = []

            for param in params:
                param_copy = param.copy()
                other.parameters[param.name] = param_copy
                other.sections[name].append(param_copy)

        return other


def get_docked_output_file_from_conf(config: GoldConfiguration) -> str:
    """Retrieve the docking output file from a GOLD configration."""
    try:
        output_file = config["concatenated_output"][0]
    except KeyError:
        output_file = f'gold_soln_{Path(config["ligand_data_file"][0]).stem}_*'

    return output_file


def get_number_of_ligands_in_file(ligand_file: Union[str, Path]) -> int:
    """Retrieve the number of ligands in a ligand file."""
    ligand_file = Path(ligand_file)

    if ligand_file.suffix not in [".mol2", ".sdf"]:
        raise IOError("Only .sdf or .mol2 formats are supported for ligand files")

    mol_separator = b"$$$$"
    if ligand_file.suffix == ".mol2":
        mol_separator = b"@<TRIPOS>MOLECULE"

    nmol = 0
    with ligand_file.open("rb", encoding="utf-8") as filereader:
        for line in filereader:
            if line.startswith(mol_separator):
                nmol += 1
    return nmol


def get_names_for_chunks(
    parent_file: Union[str, Path], nparts: int, suffix: str = None
) -> List[Path]:
    """Build the name for the chunk files."""
    parent_file = Path(parent_file)
    root = parent_file.parent
    stem = parent_file.stem
    if suffix is None:
        suffix = parent_file.suffix

    formatter = get_formatter_from_max_value(nparts)
    chunks = [root / f"{stem}_{i:{formatter}}{suffix}" for i in range(1, nparts + 1)]

    return chunks


def get_atomno_from_ligand_file(ligand_file: Union[str, Path]) -> int:
    """Retrieve the atom index (used for the covalent bond) from a ligand file."""
    ligand_file = Path(ligand_file)

    if ligand_file.suffix != ".sdf":
        raise NotImplementedError("only .sdf are supported")

    atom_no = -1
    keep_next = False
    with ligand_file.open("rb", encoding="utf-8") as filereader:
        for line in filereader:
            if line.startswith(b"> <Hit atom>"):
                keep_next = True
                continue

            if keep_next:
                atom_no = int(line.strip())
                break

    if atom_no == -1:
        raise ValueError(f"No Hit atom defined in {ligand_file}")

    return atom_no


def split_input(
    ligand_file: Union[str, Path], output_dir: Union[str, Path] = None
) -> List[Path]:
    """Split a (big) ligand file into chunks."""
    files = []

    ligand_file = Path(ligand_file)

    with EntryReader(str(ligand_file)) as mol_reader:
        n_mols = len(mol_reader)

        if output_dir is None:
            output_dir = ligand_file.parent
        else:
            output_dir = Path(output_dir)

        chunk_files = get_names_for_chunks(
            output_dir / ligand_file.name, n_mols, ".sdf"
        )

        logger.info("Splitting '%s' into %s .sdf files", ligand_file, n_mols)

        for i, mol in enumerate(mol_reader):
            chunk_file = chunk_files[i]

            with EntryWriter(chunk_file) as mol_writer:
                mol_writer.write(mol)

            files.append(chunk_file)

    return files


def split_conf(
    gold_conf: Union[Path, str],
    ligand_files: List[Path],
    output_dir: Union[Path, str] = None,
) -> List[Path]:
    """Split a GOLD configuration file into chunks."""
    gold_conf = Path(gold_conf)

    if output_dir is None:
        output_dir = gold_conf.parent
    else:
        output_dir = Path(output_dir)

    default_config = GoldConfiguration(gold_conf)

    ligand_files = [Path(filename) for filename in ligand_files]

    conf_files = get_names_for_chunks(output_dir / gold_conf.name, len(ligand_files))
    formatter = get_formatter_from_max_value(len(ligand_files))

    try:
        directory = Path(default_config["directory"][0])
    except KeyError:
        directory = Path(gold_conf.parent)

    for i, ligand_file in enumerate(ligand_files):
        task_config = default_config.copy()

        task_output_directory = directory.parent / f".tmp_output_{i + 1:{formatter}}"

        task_config["covalent_ligand_atom_no"] = [
            str(get_atomno_from_ligand_file(ligand_file)),
        ]
        task_config["ligand_data_file"][0] = str(ligand_file)
        task_config["directory"] = [
            str(task_output_directory),
        ]

        with open(conf_files[i], "w", encoding="utf-8") as filewriter:
            filewriter.write(task_config.to_string())

    return conf_files


def write_job(
    gold_conf: Union[str, Path], output_file: Union[str, Path], ntasks: int = 1
):
    """Write a job script to launch the calculation job on the HPC cluster."""
    gold_conf = Path(gold_conf)
    output_file = Path(output_file)
    config = GoldConfiguration(gold_conf)
    docking_output_dir = Path(config["directory"][0])
    docked_file = get_docked_output_file_from_conf(config)

    formatter = get_formatter_from_max_value(ntasks)

    job_skel = """#!/bin/bash
module purge
module load csds

echo ""

if [[ -z ${SGE_TASK_ID:-} ]]; then
    echo "ERROR: Not inside a SGE Array job. Exiting"
    exit 1
else
    FORMATTED_TASK_ID=$(printf "%{{FORMATTER}}" "$SGE_TASK_ID")
fi

conf_file="{{CONF}}_${FORMATTED_TASK_ID}.conf"
task_output_dir=".tmp_output_${FORMATTED_TASK_ID}"
final_output_dir="{{OUTPUT_DIR}}"

echo "Running GOLD with configuration file '${conf_file}'"

gold_auto "${conf_file}"

if [[ $? == "0" ]] ; then
    echo "GOLD execution OK"

    echo "Moving docking results from {{OUTPUT_FILE}} to ${final_output_dir}"
    mkdir -p ${final_output_dir}
    {{COPY_CMD}}
    echo "Moving gold_protein.mol2 to ${final_output_dir}"
    (
        flock -e 200
        if ! [[ -e "${final_output_dir}/gold_protein.mol2" ]]; then
            cp "${task_output_dir}/gold_protein.mol2" "${final_output_dir}/gold_protein.mol2"
        fi
    ) 200>${final_output_dir}/.lock
    echo "Deleting ${task_output_dir}"
    rm -rf "${task_output_dir}"
else
    echo "GOLD execution failed. Check log files in ${task_output_dir} for details"
fi
"""

    if "*" in docked_file:
        copy_cmd = """cp "${task_output_dir}"/{{OUTPUT_FILE}} ${final_output_dir}"""
    else:
        copy_cmd = """(
        flock -e 200
        cat "${task_output_dir}"/{{OUTPUT_FILE}} >> ${final_output_dir}/{{OUTPUT_FILE}}
    ) 200>${final_output_dir}/.lock"""
    job_content = job_skel.replace("{{CONF}}", gold_conf.stem)
    job_content = job_content.replace("{{OUTPUT_DIR}}", str(docking_output_dir))
    job_content = job_content.replace("{{FORMATTER}}", str(formatter))
    job_content = job_content.replace("{{COPY_CMD}}", copy_cmd)
    job_content = job_content.replace("{{OUTPUT_FILE}}", docked_file)

    with open(output_file, "w", encoding="utf-8") as filewriter:
        filewriter.write(job_content)


def prepare_covalent_docking(
    gold_conf: Union[str, Path], output_job: Union[str, Path]
) -> int:
    """Main function to prepare the input files for a covalent docking."""
    config = GoldConfiguration(gold_conf)
    if "covalent_ligand_atom_no" not in config.parameters:
        raise NotImplementedError(
            "Only covalent docking defined with 'covalent_ligand_atom_no' is supported"
        )

    ligand_file = Path(config["ligand_data_file"][0])
    if not ligand_file.is_file():
        ligand_file = Path(gold_conf).absolute().parent / ligand_file

    if not ligand_file.is_file():
        raise IOError(f"No such ligand file: {ligand_file}")

    ligand_files = split_input(ligand_file)

    conf_files = split_conf(gold_conf, ligand_files)

    write_job(gold_conf, output_job, len(conf_files))

    stat_result = os.stat(output_job)
    os.chmod(output_job, stat_result.st_mode | stat.S_IEXEC)

    return len(conf_files)


def load_docking_results(gold_conf: Union[str, Path]) -> ccdc.docking.Docker.Results:
    """Load the docking results for processing."""
    settings = Docker.Settings.from_file(str(gold_conf))
    docker = Docker(settings=settings)
    return docker.results


def load_substructure_searcher(
    config: GoldConfiguration,
) -> Tuple[Union[None, SubstructureSearch], int]:
    """Load a substructure searcher from the GOLD configuration."""
    try:
        substructure_filename = str(
            config.filename.parent / config["covalent_substructure_filename"][0]
        )
    except KeyError:
        logger.info(
            "No substructure filename found in conf file. "
            "Assuming single covalent link."
        )
        return None, int(config["covalent_ligand_atom_no"][0]) - 1

    with MoleculeReader(substructure_filename) as reader:
        substructure_mol = reader[0]

    substructure = MoleculeSubstructure(substructure_mol)

    for atom in substructure.atoms:
        atom.add_protein_atom_type_constraint("LIGAND")

    searcher = SubstructureSearch()
    _ = searcher.add_substructure(substructure)

    substructure_atom_index = int(config["covalent_substructure_atom_no"][0]) - 1

    return searcher, substructure_atom_index


def get_bonding_atoms(
    covalent_complex: Protein,
    searcher: Union[None, SubstructureSearch],
    linker_atom_index: int,
) -> Tuple[Atom, Atom]:
    """Identify atoms to be covalently bonded."""
    if searcher is None:
        # Determine linker atom from atom
        ligand_linker_atom = [
            x for x in covalent_complex.atoms if x.protein_atom_type == "Ligand"
        ][linker_atom_index]
    else:
        # Determine linker atom in ligand using substructure...
        matches = searcher.search(covalent_complex)
        if not matches:
            raise ValueError("Error! No ligand substructure match in complex!")

        match = matches[0]

        ligand_linker_atom = match.match_atoms()[linker_atom_index]

    logger.debug(
        "Ligand linker atom: %s/%s (%s)",
        ligand_linker_atom.residue_label,
        ligand_linker_atom.label,
        ligand_linker_atom.index + 1,
    )

    # Remove any Hs on ligand linker atom such that it is singly-connected...
    for bond in ligand_linker_atom.bonds:
        x_atom = [atom for atom in bond.atoms if atom != ligand_linker_atom][0]
        if x_atom.atomic_number == 1:
            covalent_complex.remove_atom(x_atom)

    if len(ligand_linker_atom.bonds) != 1:
        raise ValueError(
            f"Error! Ligand linker atom has more than one bond! "
            f"({len(ligand_linker_atom.bonds)} bonds)"
        )

    # Get the remaining ligand atom attached to the linker atom...
    ligand_atom = [
        atom for atom in ligand_linker_atom.bonds[0].atoms if atom != ligand_linker_atom
    ][0]

    return ligand_linker_atom, ligand_atom


def build_complexes_from_results(
    results: ccdc.docking.Docker.Results,
    config: GoldConfiguration,
) -> List[Protein]:
    """Build the protein/ligand complexes from the docked poses."""
    try:
        protein_atom_index = int(config["covalent_protein_atom_no"][0]) - 1
    except KeyError as raised_exc:
        raise KeyError(
            "Could not find the protein to use for covalent bonding!"
        ) from raised_exc

    searcher, linker_atom_index = load_substructure_searcher(config)

    complexes = []

    logger.info("Building complexes from the protein and the docked ligands")
    for num, solution in enumerate(results.ligands, 1):
        # Make a complex from the solution...
        covalent_complex = results.make_complex(solution).copy()
        covalent_complex.remove_unknown_atoms()  # Remove lone pairs for export

        # Determine linker atom in protein (from conf file)...
        protein_linker_atom = covalent_complex.atoms[protein_atom_index]

        if len(protein_linker_atom.bonds) != 1:
            raise ValueError(
                f"Error! Protein linker atom has more than one bond! "
                f"({len(protein_linker_atom.bonds)} bonds)"
            )
        logger.debug(
            "Protein linker atom: %s/%s (%s)",
            protein_linker_atom.residue_label,
            protein_linker_atom.label,
            protein_linker_atom.index + 1,
        )

        # Retrieve bonding atoms
        ligand_linker_atom, ligand_atom = get_bonding_atoms(
            covalent_complex, searcher, linker_atom_index
        )

        # Remove the now-superfluous ligand linker atom...
        covalent_complex.remove_atom(ligand_linker_atom)

        # Attach protein linker atom to ligand atom...
        covalent_complex.add_bond(1, protein_linker_atom, ligand_atom)

        # Change identifier for better identification
        complex_id = get_complex_id_from_pose(solution)
        covalent_complex.identifier = complex_id

        complexes.append(covalent_complex)

        logger.info("Complex %s/%s created", num, len(results.ligands))

    return complexes


def get_complex_id_from_pose(pose: Union[Entry, Docker.Results.DockedLigand]) -> str:
    """Retrieve the complex ID from the docked pose."""
    ligand_id = pose.identifier
    protein_id = pose.attributes["Gold.Id.Protein"].split("|")[0]

    return f"{protein_id}+{ligand_id}"


def write_complexes_from_conf(
    gold_conf: Union[str, Path], output_file: Union[str, Path] = "covalent_complex.mol2"
) -> List[str]:
    """Save protein/ligand complexes to mol2 files."""
    config = GoldConfiguration(gold_conf)
    results = load_docking_results(gold_conf)

    complexes = build_complexes_from_results(results, config)

    logger.info("Now saving %s complexes:", len(complexes))
    try:
        fname, fext = os.path.splitext(output_file)
    except IndexError:
        fname = str(output_file)
        fext = ".mol2"
    output_files = [f"{fname}_{i + 1:05d}{fext}" for i in range(len(complexes))]

    for i, covalent_complex in enumerate(complexes):
        file_path = Path(output_files[i])
        file_path.parent.mkdir(parents=True, exist_ok=True)
        with EntryWriter(str(file_path)) as writer:
            writer.write(covalent_complex)
        logger.info(
            "Complex '%s' written to %s.", covalent_complex.identifier, file_path
        )

    return output_files


def load_ligands_from_file(file_name: Union[str, Path]) -> List[Entry]:
    """Load ligands from a ligand file."""
    entries = []

    with EntryReader(file_name) as reader:
        for entry in reader:
            entries.append(entry)

    return entries


def build_covalent_complex(
    protein: Protein,
    ligand: Entry,
    protein_atom_no: int,
    ligand_atom_no: int,
) -> Protein:
    """Build a protein/ligand complex from docking results."""
    covalent_complex = protein.copy()

    covalent_complex.add_ligand(ligand.molecule)

    covalent_complex.remove_unknown_atoms()

    protein_linker_atom = covalent_complex.atoms[protein_atom_no]
    if len(protein_linker_atom.bonds) == 1:
        raise ValueError(
            f"Error! Protein linker atom has more than one bond! "
            f"({len(protein_linker_atom.bonds)} bonds)"
        )

    # Retrieve bonding atoms
    ligand_linker_atom, ligand_atom = get_bonding_atoms(
        covalent_complex, None, ligand_atom_no
    )

    # Remove the now-superfluous ligand linker atom...
    covalent_complex.remove_atom(ligand_linker_atom)

    # Attach protein linker atom to ligand atom...
    covalent_complex.add_bond(1, protein_linker_atom, ligand_atom)

    # Change identifier for better identification
    complex_id = get_complex_id_from_pose(ligand)
    covalent_complex.identifier = complex_id

    return covalent_complex


def process_docking_results_to_databases(
    gold_conf: Union[str, Path],
    ligand_db: Union[str, Path],
    complex_db: Union[str, Path],
    size_limit: int = 5000,
) -> None:
    """Save docking results to databases."""
    gold_conf = Path(gold_conf)
    ligand_db = Path(ligand_db)
    complex_db = Path(complex_db)

    config = GoldConfiguration(gold_conf)

    try:
        covalent_protein_atom_no = int(config["covalent_protein_atom_no"][0]) - 1
    except KeyError as raised_exc:
        raise KeyError(
            f"{gold_conf} does not contain covalent docking options"
        ) from raised_exc

    docking_output_directory = gold_conf.parent / config["directory"][0]

    ligand_db_orig = Path(config["ligand_data_file"][0])
    if not ligand_db_orig.exists():
        ligand_db_orig = gold_conf.parent / ligand_db_orig
    if not ligand_db_orig.exists():
        raise IOError(f"No such ligand database: {ligand_db_orig}")

    docked_output_file_glob = str(
        docking_output_directory / get_docked_output_file_from_conf(config)
    )
    docked_output_files = glob.glob(docked_output_file_glob)

    protein_file = str(docking_output_directory / "gold_protein.mol2")
    logger.info("Loading protein from %s...", protein_file)
    protein = Protein.from_file(protein_file)
    logger.info("Protein loaded")

    ligands = []
    ligand_binding_atomno = []
    logger.info("Loading docked poses from %s files...", len(docked_output_files))
    for file_name in docked_output_files:
        for ligand in load_ligands_from_file(file_name):
            try:
                ligand_binding_atomno.append(int(ligand.attributes["Hit atom"]) - 1)
            except KeyError as raised_exc:
                raise KeyError(
                    f"Poses in {file_name} do not contain 'Hit atom'"
                ) from raised_exc
            ligand.attributes["complex_id"] = get_complex_id_from_pose(ligand)
            ligands.append(ligand)
    logger.info("%s ligands loaded!", len(ligands))

    n_parts = math.ceil(len(ligands) / size_limit)
    logger.info(
        "Given the size limit of %s, %s and %s will be splitted in %s parts",
        size_limit,
        ligand_db,
        complex_db,
        n_parts,
    )

    logger.info("Processing the covalent complexes... (This takes a long time)")
    current_part = 0
    n_saved = 0
    complex_writer = None
    ligand_writer = None
    if n_parts == 1:
        complex_writer = EntryWriter(f"{complex_db}")
        ligand_writer = EntryWriter(f"{ligand_db}")
    next_tick = 10
    for i, ligand in enumerate(ligands):
        covalent_complex = build_covalent_complex(
            protein, ligand, covalent_protein_atom_no, ligand_binding_atomno[i]
        )

        if n_saved == size_limit or (i == 0 and n_parts > 1):
            if complex_writer is not None:
                complex_writer.close()

            if ligand_writer is not None:
                ligand_writer.close()

            if n_saved > 0:
                logger.info(
                    "%s complexes and ligands saved to %s and %s, respectively",
                    n_saved,
                    complex_writer.file_name,
                    ligand_writer.file_name,
                )
            n_saved = 0

            current_part += 1
            complex_writer = EntryWriter(
                f"{complex_db.parent / complex_db.stem}_{current_part:{get_formatter_from_max_value(n_parts)}}"
                f"{complex_db.suffix}"
            )
            ligand_writer = EntryWriter(
                f"{ligand_db.parent / ligand_db.stem}_{current_part:{get_formatter_from_max_value(n_parts)}}"
                f"{ligand_db.suffix}"
            )

        complex_writer.write(covalent_complex)
        ligand_writer.write(ligand)
        n_saved += 1

        percent_done = int((i + 1) / len(ligands) * 100)
        if percent_done >= next_tick:
            logger.info("Processing %s %% done", next_tick)
            next_tick += 10

    complex_writer.close()
    ligand_writer.close()
    if n_saved > 0:
        logger.info(
            "%s complexes and ligands saved to %s and %s, respectively",
            n_saved,
            complex_writer.file_name,
            ligand_writer.file_name,
        )

    logger.info("The results from the covalent docking are processed!")


def main(sys_args: Optional[List[str]] = None) -> NoReturn:
    """Main function called from CLI."""
    if sys_args is None:
        sys_args = sys.argv[1:]

    desc = f"""Helper script to prepare and process results from covalent docking.
For questions and/or suggestions, please contact {__author__}
"""

    parser = argparse.ArgumentParser(prog=Path(__file__).name, description=desc)
    parser.add_argument(
        "--version", action="version", version=f"%(prog)s {__version__}"
    )

    subparsers = parser.add_subparsers(
        dest="subcommand", title="Subcommands", help="Valid subcommands"
    )

    subparser_prepare = subparsers.add_parser(
        "prepare",
        description="Prepare covalent docking files based on a GOLD "
        "configuration template.",
    )
    subparser_prepare.add_argument(
        "--conf",
        "-c",
        help="GOLD configuration file.(default: 'gold.conf')",
        default="gold.conf",
    )
    subparser_prepare.add_argument(
        "--output-job",
        "-o",
        help="Name of the generated SGE job script.(default: 'run_my_covalent_docking.sh')",
        default="run_my_covalent_docking.sh",
    )

    subparser_process = subparsers.add_parser(
        "process",
        description="Process results from covalent docking and "
        "generates two files: one with the docked "
        "poses and one for with the covalent "
        "complexes.",
    )
    subparser_process.add_argument(
        "--conf",
        "-c",
        help="GOLD configuration file.(default: 'gold.conf')",
        default="gold.conf",
    )
    subparser_process.add_argument(
        "--output-complexes",
        "-o",
        help="Name of output file for the covalent complexes.(default: 'complexes.mol2')",
        default="complexes.mol2",
    )
    subparser_process.add_argument(
        "--output-poses",
        "-p",
        help="Name of output file for the docked poses.(default: 'poses.sdf')",
        default="poses.sdf",
    )
    subparser_process.add_argument(
        "--size-limit",
        "-s",
        help="Maximum number of entries is files before trigerring chunk generation",
        type=int,
        default=5000,
    )

    args = parser.parse_args(sys_args)

    active_command = args.subcommand

    if active_command == "prepare":
        njobs = prepare_covalent_docking(args.conf, args.output_job)
        print(
            f"To run you docking job run the following command:\n"
            f"\nqsub -q cpu -t 1-{njobs} -wd $(pwd) -o gold.log -tc 50 -j y {args.output_job}\n"
        )

    elif active_command == "process":
        process_docking_results_to_databases(
            args.conf, args.output_poses, args.output_complexes, args.size_limit
        )
    else:
        parser.print_usage()
        print(
            "error: you must specify a subcommand among the following ones: prepare, process",
            file=sys.stderr,
        )
        sys.exit(2)
    sys.exit(0)


if __name__ == "__main__":
    main()
