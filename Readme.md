# How to use `covalent_docking_helper.py`

This script is a helper that is to be used with the associated [Knime Workflow](https://hub.knime.com/emilie_pihan/spaces/Evotec/latest/Evotec_Covalent_Processing_forGold~FvOexZK0ynxgyFms).
## Prerequisites

The following files are needed:

1. a GOLD configuration that is consistent with "atom-based" (as opposed to "substructure-based") covalent docking
2. a ligand database provided as a .sdf file that contains a filed named 'Hit atom' to specify the atom number of the ligand's atom bound to the protein.

## Prepare the files to run the docking

Run `covalent_docking_helper.py` with the sub command `prepare`.
You can specify the GOLD configuration file with the option `--conf` (or `-c`).
If not specified, the script will search for a `gold.conf` file.
You may also specify the name of the output script that will be written for being submitted to SGE with the option `--output-job` (or `-o`).
If not specifid, a job script named `run_my_covalent_docking.sh` will be created.

Here is an example:

```shell
python covalent_docking_helper.py prepare --conf gold.conf -o job.sh
```

*Note:* In addition to `job.sh`, the script will also create many `gold_XXXXX.conf` (where XXXXX is number up to 99999) and as many `input_XXXXX.sdf` (provided the ligand database is `input.sdf`).

On exit, the script will specify the command to be used to submit the docking to SGE.
The command will look like this one:

```shell
qsub -t 1-120 -wd $(pwd) -o gold.log -tc 50 -j y job.sh
```

*Note:* To ease the footprint on the cluster, this command will run no more than 50 concurrent docking jobs (`-tc` option).

## Process results from covalent docking

When the docking was run successfully, run `covalent_docking_helper.py` with the sub command `process`.
You need to specify the original GOLD configuration file with the option `--conf` (or `-c`). If not specified, the script will search for
a `gold.conf` file.
You may also specify the name of the two output files:

1. `--output-complexes` (or `-o`) to specify where to save the covalent complexes (docked ligand+protein). Default file is `complexes.mol2`.

2. `--output-poses` (or `-p`) to specify where to save the concatenated docked poses with the docking results (ligand only, no protein there). Default file is `poses.sdf`.

Here is an example:

```shell
python covalent_docking_helper.py process --conf gold_atom_sdf.conf -o complexes.mol2 -p poses.sdf
```

Once done, MOE can be used to merge the database

## Merge the created database with MOE

1. Open `poses.sdf` with MOE and create a database named `poses.mdb`.

2. Create a database named `complexes.mdb` and import `complexes.mol2` in it and rename the field `mol` to `complex`

3. Create a new field in `complexes.mdb` named `complex_id` that contain the extracted name for the field `complex`.
   (right click on the `complex` field, then `Name`>`Extract...`)

4. Merge `poses.mdb` and `complexes.mdb` into `docking.mdb` using MOE's Database Viewer: `File` > `Merge...`.
   Use the field named `complex_id` (present in both databases) as the key to identify related entries.
   There should be no duplicates entries nor ones that are not present in both databases.

5. Simply open `docking.mdb` to browse the docking results that contains everything!
